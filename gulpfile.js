var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});
var config = require('./gulp.config')();

gulp.task('vet', function () {
    return gulp
        .src(config.allJs)
        .pipe($.plumber())
        .pipe($.jshint())
        .pipe($.jscs())
        .pipe($.jshint.reporter('jshint-stylish', {verbose: true}));
});

gulp.task('watch', function() {
    // gulp.watch(config.allJs, ['vet']);
    gulp.watch(config.allJs, ['vet']);
});

gulp.task('copy-bower-files', function() {
    var mainBowerFiles = require('main-bower-files');

    return gulp.src(mainBowerFiles(), {base: './bower_components'})
        .pipe($.print())
        .pipe(gulp.dest('./build/lib'));
});

gulp.task('copy-app', function() {
    return gulp.src('./src/client/**/*.*')
        .pipe(gulp.dest('./build'));
});

gulp.task('inject-libs', function() {

    var mainBowerFiles = require('main-bower-files');

    var files = gulp.src(['./build/lib/**/*.js', './build/app/**/*.js']);

    return gulp
        .src(config.index)
        .pipe($.inject(files, {ignorePath: '/build/', addRootSlash: false}))
        .pipe(gulp.dest('./build'));
});

gulp.task('inject-code', function() {
    return gulp
        .src('./build/index.html')
        .pipe($.inject(gulp.src('./build/app/**/*.js', {base: 'build/'})))
        .pipe(gulp.dest('./build'));
});

gulp.task('bower', function() {
    var wiredep = require('wiredep');
    var bowerFiles = wiredep({devDependencies: true})['js'];

    return gulp
        .src(bowerFiles)
        .pipe($.print());
});

gulp.task('wiredep', function() {
    var options = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;

    return gulp
        .src(config.index)
        .pipe(wiredep(options))
        .pipe($.replace(/bower_components/g, 'lib'))
        .pipe($.inject(gulp.src(config.js), {ignorePath: '/', addRootSlash: false}))
        .pipe(gulp.dest(config.client));
});

gulp.task('test', function(done) {
    startTests(false /* singleRun */, done);
});

///////////////

function startTests(singleRun, done) {
    var karma = require('karma').server;
    var excludeFiles = [];

    karma.start({
        configFile: __dirname + '/karma.conf.js',
        exclude: excludeFiles,
        // singleRun: singleRun
    }, karmaCompleted);

    function karmaCompleted(karmaResult) {
        // log('Karma completed!');
        if (karmaResult === 1) {
            done('karma: tests failed with code ' + karmaResult);
        } else {
            done();
        }
    }
}
