module.exports = function (config) {
    var gulpConfig = require('./gulp.config')();

    config.set({

        // basePath: '../',

        files: gulpConfig.karma.files,

        exclude: [
            // 'control/src/main/webapp/libs/bootstrap.min.js',
        ],

        autoWatch: false,
        singleRun: true,

        frameworks: ['jasmine'],

        browsers: ['PhantomJS'],

        plugins: [
            'karma-phantomjs-launcher',
            'karma-jasmine'
        ]

    });
};
